# mastofeeds
Newsfeed Bot for Mastodon

Publish to Mastodon's server the rss feed of your choice.

### Dependencies

-   **Python 3**
-   Postgresql server
-   Mastodon's bot account

### Usage:

Within Python Virtual Environment:

1. Run `pip install -r requirements.txt` to install all needed libraries.

2. Run `python db-setup.py` to create needed database and table. It's needed to control what feeds are already published. db-setup.py
   will also ask you the feed's url.

3. Run `python setup.py` to get your bot's access token of an existing user. It will be saved to 'secrets/secrets.txt' for further use.

4. Run `python mastofeeds.py` to start publishing feeds.

5. Use your favourite scheduling method to set `python mastofeeds.py` to run regularly. 

